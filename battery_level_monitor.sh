#! /bin/bash
# battery_level_monitor.sh

# checking frequency (minutes)
TIMEOUT=1

ALARM_TRESHOLD=30
POWEROFF_TRESHOLD=10

export battery_status_tmp=$(mktemp)
echo "OK" > $battery_status_tmp

function get_battery_status() {
    acpi | cut -d, -f2 | tr -d \%
}

function is_battery_charging() {
    acpi | cut -d" " -f3 | tr -d "\%,"
}

function battery_critic() {
    ########################################################################
    read status < $battery_status_tmp
    [[ "$status" = "KO" ]] && return
    # The part below will be executeed only the first time
    ########################################################################

    # Save state (for the next call)
    echo "KO" > $battery_status_tmp

    sudo poweroff
}

function battery_low() {
    ########################################################################
    # This part of the function will be executed each time between
    # ALARM_TRESHOLD AND POWEROFF_TRESHOLD
    ########################################################################

    # Notify $USER
    notify-send Message "BATTERIE FAIBLE !" -i "$PWD"/icone.png


    ########################################################################
    read status < $battery_status_tmp
    [[ "$status" = "LOW" ]] && return
    # The part below will be executeed only the first time
    ########################################################################

    # Save state (for the next call)
    echo "LOW" > $battery_status_tmp
}

cd $(dirname $0)

# Test if battery tool is installed. Exit if it is not
$(which acpi) > /dev/null || exit 1

while [ 1 -eq 1 ]
do
    BAT=$(get_battery_status)
    POWERED=$(is_battery_charging)
    if [ "$POWERED" = "Charging" ] 
    then 
        break
    elif [ $BAT -le $POWEROFF_TRESHOLD ] 
    then 
        battery_critic
    elif [ $BAT -le $ALARM_TRESHOLD ]
    then
        battery_low
    fi
    sleep $((TIMEOUT * 60))
done

exit 0
